import React from "react"

import Section from "../../../components/section"

export default function Footer() {
  return <Section bgColor="#2a3a3f"></Section>
}
