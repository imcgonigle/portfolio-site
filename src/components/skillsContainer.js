import React from "react"
import skillsContainerStyles from "./skillsContainer.module.css"

export default function SkillsContainer({ children }) {
  return <div className={skillsContainerStyles.skillsContainer}>{children}</div>
}
